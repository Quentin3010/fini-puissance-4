// Définir les paramètres des cercles
var radius = 30;
var colors = ['red', 'yellow'];

// Fonction pour créer un cercle
function createCircle() {
    var circle = document.createElement('div');
    circle.classList.add('circle');
    circle.style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];
    circle.style.width = radius * 2 + 'px';
    circle.style.height = radius * 2 + 'px';
    circle.style.left = Math.random() * (window.innerWidth - radius * 2) + 'px';
    circle.style.animationDuration = Math.random() * 3 + 2 + 's';
    document.getElementById('cercles-container').appendChild(circle);
    setTimeout(function () {
        circle.remove();
    }, getCadence()*10);
}

setInterval(createCircle, getCadence());

// Fonction pour obtenir la cadence des cercles en fonction de la largeur de l'écran
function getCadence() {
    var screenWidth = window.innerWidth;
    if (screenWidth >= 1200) {
        return 500; // cadence de 4 cercles par seconde
    } else if (screenWidth >= 768) {
        return 1000; // cadence de 2 cercles par seconde
    } else {
        return 1500; // cadence de 1 cercle par seconde
    }
}