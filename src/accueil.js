//Constantes de document.(...)
const BoutonRegles = document.getElementById("BoutonRegles")
//const BoutonDebug = document.getElementById("BoutonDebug")
//Variables
let reglesShow = false;

/*BoutonDebug.addEventListener("click", function () {
    PlateauSection.setAttribute("style", "display:show");
    AcceuilSection.setAttribute("style", "display:show");
    EcranFinSection.setAttribute("style", "display:show");
});*/

BoutonRegles.addEventListener("click", function () {
    if (reglesShow === false) {
        ReglesSection.setAttribute("style", "display:show");
        reglesShow = true;
    } else {
        ReglesSection.setAttribute("style", "display:none");
        reglesShow = false;
    }
});

ReglesSection.addEventListener("click", function () {
    if (reglesShow === true) {
        ReglesSection.setAttribute("style", "display:none");
        reglesShow = false;
    }
});