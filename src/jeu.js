//Constantes de document.(...)
const AcceuilSection = document.getElementById("AcceuilSection")
const ReglesSection = document.getElementById("ReglesSection")
const PlateauSection = document.getElementById("PlateauSection")
const EcranFinSection = document.getElementById("EcranFinSection")
const BoutonPlay = document.getElementById("BoutonPlay")
const InputJoueur1 = document.getElementById("InputJoueur1")
const InputJoueur2 = document.getElementById("InputJoueur2")
const SelecteurColonne = document.getElementById("SelecteurColonne")
const Plateau = document.getElementById("Plateau")
const NumPartie = document.getElementById("NumPartie")
const VictoireJ1 = document.getElementById("VictoireJ1")
const VictoireJ2 = document.getElementById("VictoireJ2")
const NumTour = document.getElementById("NumTour")
const MessageDeVictoire = document.getElementById("MessageDeVictoire")
const PlateauEcranFin = document.getElementById("PlateauEcranFin")
const BoutonRejouer = document.getElementById("BoutonRejouer")
const BoutonQuitter = document.getElementById("BoutonQuitter")
const J1 = document.getElementById("J1")
const J2 = document.getElementById("J2")
let casePlateau;
//Variables importantes
const nbCol = 7;
const nbLig = 6;
let pseudoJ1;
let pseudoJ2;
let nbPartie = 0;
let nbTour = 0;
let lastLig = 0;
let lastCol = 0;
let egalite = false;
let msgVic;
let NbVictoireJ1 = 0;
let NbVictoireJ2 = 0


/////////////////////
// ACCUEIL SECTION //
/////////////////////

BoutonPlay.addEventListener("click", function () {
    InputJoueur1.classList.remove("red-border");
    InputJoueur2.classList.remove("red-border");

    if (InputJoueur1.value !== "" && InputJoueur2.value !== "" && InputJoueur1.value.length<=16 && InputJoueur2.value.length<=16) {
        pseudoJ1 = InputJoueur1.value;
        pseudoJ2 = InputJoueur2.value;

        startGame();

        PlateauSection.setAttribute("style", "display:show");
        AcceuilSection.setAttribute("style", "display:none");
    } else {
        if (InputJoueur1.value === "" || InputJoueur1.value.length>16) {
            InputJoueur1.classList.add("red-border");
        }
        if (InputJoueur2.value === "" || InputJoueur2.value.length>16) {
            InputJoueur2.classList.add("red-border");
        }
    }
});

/////////////////////
// PLATEAU SECTION //
/////////////////////

function genererPlateauVide() {
    let res = "<tbody>";
    //PLateau de jeu vide
    for (let i = nbLig - 1; i >= 0; i--) {
        res += `<tr>`;
        for (let j = 0; j < nbCol; j++) {
            res += `<td> <span id="${i}:${j}" class="vide"></span> </td> `;
        }
        res += `</tr>`;
    }
    res += "</tbody>";
    Plateau.innerHTML = res;
}

function ajouterPion(numCol) {
    for (let i = 0; i < nbLig; i++) {
        casePlateau = document.getElementById(`${i}:${numCol}`);
        if (casePlateau.getAttribute('class') === "vide") {
            if (nbTour % 2 == 1) casePlateau.setAttribute('class', 'red')
            else casePlateau.setAttribute('class', 'yellow')
            lastLig = i;
            lastCol = numCol;
            return true;
        }
    }
    return false;
}

function genererBoutons() {
    let res = "<tbody>";
    //Boutons pour interragir avec une colonne
    for (let j = 0; j < nbCol; j++) {
        res += `<td><button class="BoutonCol" value="${j}"></button></td>`;
    }
    res += "</tbody>";
    SelecteurColonne.innerHTML = res

    const BoutonsCol = document.querySelectorAll(".BoutonCol");
    BoutonsCol.forEach(BoutonCol => {
        BoutonCol.addEventListener('click', function () {
            //Teste et ajoute le pion si possible
            if (ajouterPion(BoutonCol.value) == true) {
                //Verif condition de victoire
                if (testVictoire() == true) {
                    finDePartie();

                    PlateauSection.setAttribute("style", "display:none");
                    EcranFinSection.setAttribute("style", "display:show");
                }
                //Passe au tour suivant
                tourSuivant();
            }
        });
    });
}

function testVictoire() {
    //Parcours horizontale
    let nbPion = 1;
    nbPion += parcours(lastLig, lastCol, 0, -1);//gauche
    nbPion += parcours(lastLig, lastCol, 0, 1);//droite
    if (nbPion >= 4) return true
    //Parcours verticale
    nbPion = 1;
    nbPion += parcours(lastLig, lastCol, 1, 0);//haut
    nbPion += parcours(lastLig, lastCol, -1, 0);//bas
    if (nbPion >= 4) return true
    //Parcours diagonale 1
    nbPion = 1;
    nbPion += parcours(lastLig, lastCol, 1, -1);//haut gauche
    nbPion += parcours(lastLig, lastCol, -1, 1);//bas droite
    if (nbPion >= 4) return true
    //Parcours diagonale 2
    nbPion = 1;
    nbPion += parcours(lastLig, lastCol, -1, -1);//bas gauche
    nbPion += parcours(lastLig, lastCol, 1, 1);//haut droite
    if (nbPion >= 4) return true
    else if (verifLigneHautPleine() == true) return true
    else return false;
}

function parcours(lig, col, addLig, addCol){
    casePlateau = document.getElementById(`${parseInt(lig)+parseInt(addLig)}:${parseInt(col)+parseInt(addCol)}`);

    if(casePlateau===null){
        return 0;
    }
    else if(nbTour%2==1 && casePlateau.getAttribute('class')==="red"){
        return parcours(parseInt(lig)+parseInt(addLig), parseInt(col)+parseInt(addCol), addLig, addCol) + 1
    } 
    else if(nbTour%2==0 && casePlateau.getAttribute('class')==="yellow"){
        return parcours(parseInt(lig)+parseInt(addLig), parseInt(col)+parseInt(addCol), addLig, addCol) + 1
    } 
    else{
        return 0;
    }
}


//Si la ligne du haut est pleine c'est que la partie est fini sur un égalité
function verifLigneHautPleine() {
    for (let j = 0; j < nbCol; j++) {
        casePlateau = document.getElementById(`${nbLig - 1}:${j}`);
        if (casePlateau.getAttribute('class') === "vide") return false;
    }
    egalite = true;
    return true;
}

function tourSuivant() {
    nbTour++;
    NumTour.innerHTML = nbTour;
    J1.classList.remove("red-text")
    J2.classList.remove("yellow-text")
    if(nbTour%2==1){
        J1.classList.add("red-text")
    }else{
        J2.classList.add("yellow-text")
    }
}

function resetParam() {
    //Paramètres de début de partie
    NumPartie.innerHTML = nbPartie;
    nbTour = 0
    NumTour.innerHTML = nbTour;
}

function setPseudos() {
    const pseudo1 = document.querySelectorAll(".Pseudo1");
    pseudo1.forEach(p => {
        p.innerHTML = pseudoJ1;
    });
    const pseudo2 = document.querySelectorAll(".Pseudo2");
    pseudo2.forEach(p => {
        p.innerHTML = pseudoJ2;
    });
}

function finDePartie() {
    MessageDeVictoire.classList.remove("red-text")
    MessageDeVictoire.classList.remove("yellow-text")

    if (egalite == true) {
        msgVic = "Egalité";
    }
    else if (nbTour % 2 == 1) {
        msgVic = "Victoire de " + pseudoJ1 + " !"
        NbVictoireJ1++;
        VictoireJ1.innerHTML = NbVictoireJ1;
        MessageDeVictoire.classList.add("red-text")
    } else {
        msgVic = "Victoire de " + pseudoJ2 + " !"
        NbVictoireJ2++;
        VictoireJ2.innerHTML = NbVictoireJ2;
        MessageDeVictoire.classList.add("yellow-text")
    }
    MessageDeVictoire.innerHTML = msgVic
    PlateauEcranFin.innerHTML = Plateau.innerHTML
}

function startGame() {
    egalite = false;
    nbPartie++;
    resetParam();
    tourSuivant();
    setPseudos();
    genererBoutons()
    genererPlateauVide()
}

//////////////////////////
// ECRAN DE FIN SECTION //
//////////////////////////

BoutonRejouer.addEventListener("click", function () {
    startGame();

    PlateauSection.setAttribute("style", "display:show");
    EcranFinSection.setAttribute("style", "display:none");
})

BoutonQuitter.addEventListener("click", function () {
    nbPartie = 0;
    nbTour = 0;
    NbVictoireJ1 = 0;
    NbVictoireJ2 = 0

    AcceuilSection.setAttribute("style", "display:show");
    EcranFinSection.setAttribute("style", "display:none");
})

//Start
genererPlateauVide();
