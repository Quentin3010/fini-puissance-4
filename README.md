# Puissance 4

Quentin BERNARD & Clément HEYD

## Description du projet

Le projet "Puissance 4" est une adaptation du célèbre jeu du même nom, réalisée sous forme d'un site web interactif. Développé en utilisant le langage JavaScript, ce projet a été réalisé dans le cadre de ma formation scolaire. 

Le jeu propose une expérience en ligne du Puissance 4, où les joueurs peuvent s'affronter en local, en plaçant tour à tour leurs jetons dans une grille verticale. L'objectif est d'aligner quatre jetons de sa propre couleur horizontalement, verticalement ou en diagonale avant l'adversaire. 

Vidéo de présentation : lien
Lien du site : http://193.70.40.178/puissance4

## Fonctionnalités

- Vérification des champs de saisie des noms des joueurs et démarrage du jeu si les conditions sont remplies.
- Affichage ou masquage des règles du jeu en fonction de l'état actuel.
- Création d'un cercle avec des caractéristiques spécifiques et ajout au conteneur.
- Calcul de la cadence de génération des cercles en fonction de la largeur de l'écran.
- Génération d'un plateau de jeu vide.
- Ajout d'un pion dans une colonne spécifiée sur le plateau.
- Génération de boutons interactifs pour chaque colonne du plateau.
- Vérification des conditions de victoire après l'ajout d'un pion.
- Passage au tour suivant et mise à jour des variables de suivi.
- Réinitialisation des paramètres du jeu pour une nouvelle partie.
- Affichage des pseudos des joueurs.
- Gestion de la fin de partie avec affichage d'un message de victoire ou de match nul.
- Lancement du jeu en cachant la section d'accueil et affichant la section de jeu avec l'initialisation des paramètres nécessaires.

## Mon rôle

Malheureusement, dans le cadre de ce projet, mon partenaire n'était pas aussi investi que nécessaire, ce qui a entraîné une répartition inégale des responsabilités. 

Cependant, j'ai pris l'initiative d'assumer l'ensemble des tâches pour veiller à ce que le projet avance malgré les difficultés et le temps très court pour réaliser le projet. 

J'ai pris en charge la conception, le développement et l'implémentation des fonctionnalités essentielles du site, en m'assurant de leur bonne intégration. J'ai fait de mon mieux pour livrer un résultat satisfaisant dans les limites de la situation.



